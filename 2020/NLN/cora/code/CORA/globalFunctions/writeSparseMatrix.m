function empty = writeSparseMatrix(M,var,fid)


%write each row
[row,col] = find(M~=0);

if ~isempty(row)

    empty = 0;
    
    for i=1:length(row)
        iRow = row(i);
        iCol = col(i);
        str=bracketSubs(char(M(iRow,iCol)));
        str=[var,'(',num2str(iRow),',',num2str(iCol),') = ',str,';'];
        %write in file
        fprintf(fid, '%s\n', str);
    end
    
else
    empty = 1;
end