# VenMAS

VenMAS is a tool for verification of closed-loop systems with neural network components.
A closed-loop system consists of an environment and a number of agents, who interact with each other and 
with the environment and thus update its state. 
It is assumed that the agents' logic is partially or fully implemented using ReLU-based neural networks. 
The environment's transition function is, in general, assumed to be a piecewise linear function, and, 
in particular, can be implemented via neural networks.
The current version supports a single agent situated and acting upon a non-deterministic environment,
that is, when the outcome of the agent's action cannot be deterministically defined. 
The agent can be a stateless agent implemented through feed-forward neural networks, 
or a stateful agent implemented via recurrent neural networks. 

VenMAS takes as input a specification of the agent, of the environment and a property against which the closed-loop
system is to be verified. 
The property should be a temporal formula expressible in bounded CTL (computational tree logic). 
This includes safety properties stating that in all possible evolutions of the system within k time steps,
the system remains within the safe region.
Whenever a property is not satisfied, VenMAS produces counter-examples. 

VenMAS works by encoding the given problem as the MILP (mixed-integer linear programming) feasibility problem 
and relies in Gurobi as the backend MILP solver.

VenMAS supports feed-forward and recurrent neural networks with ReLU activation function, 
in the Keras and .nnet formats.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

In order to run this code, you will need to have installed 

* Python >= 3.5
* Gurobi 9.0.1

To be able to use Gurobi, you will need to have a Gurobi license installed on your machine.
Please follow the instructions described [here](https://www.gurobi.com/documentation/9.0/quickstart_linux/index.html).

Finally, install additional dependencies needed to run VenMAS by running the following command

		pip install -r requirements.txt

This will install Keras and Tensforflow libraries needed to manipulate neural networks.


## Running the ARCH-COMP 2020 benchmarks

No Docker container has been provided as there are problems setting it up
to work with Gurobi.

Test scripts to run ARCH-COMP 2020 benchmarks can be found in the /test folder.

To run a single benchmark run, e.g., 

		python3 run_tora.py

This will run verification for the TORA benchmark according to the specifications contained
in the competition report. Verification results are logged at "test/arch-comp.log".

We also provide a script to run all benchmarks for which results were reported in the competition
report. To run this, simply type

		./run_all_benchmarks

This will run all benchmarks contained in the test folder. The output of each benchmark is 
redirected to a corresponding .txt file, while general verification results are logged into
a common log file at "test/arch-comp.log".

The log file related to the experiments reported in the ARCH-COMP report can be found at
"submitted_results/arch-comp.log".


## Authors

* Michael Akintunde 
* Elena Botoeva
* Andreaa Kervochian
* Panagiotis Kouvaros
* Francesco Leofante
* Alessio Lomuscio
* Lalit Maganti



