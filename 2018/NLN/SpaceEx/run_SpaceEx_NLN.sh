#!/bin/bash 
function run {
	echo $2 ":"
	spaceex -g $1$2.cfg -m $1$3.xml -vl | grep 'Forbidden\|Computing reachable states done after'
}

function graphplot {
	echo $2 ":"
	spaceex -g $1$2.cfg -m $1$3.xml -o $1$2.gen -vl | grep 'Forbidden\|Computing reachable states done after'
	graph -Tpng --bitmap-size 1024x1024 -C -B -q0.5 $1$2.gen > $1$2.png
}

echo "Van der Pol"
cd VanDerPol
spaceex-zonotope -g vanderpol-zono.cfg -m vanderpol-pseudo.xml 
spaceex-zonotope -g vanderpol-zono-plot.cfg -m vanderpol-pseudo.xml -o out-zono.gen 
graph -T png -C -B -q0.5  --bitmap-size 1024x1024 out-zono.gen > vdp-zono.png
cd ..

echo "Laub-Loomis"
cd LaubLoomis
spaceex-zonotope -g laub-zono.cfg -m laub.xml 
spaceex-zonotope -g laub-zono-plot.cfg -m laub.xml -o out_0.01.gen 
graph -T png -C -B -q0.5  --bitmap-size 1024x1024 out_0.01.gen > laub_zono_0.01_plot.png 
cd ..

echo "Quadrotor"
cd Quadrotor
spaceex-zonotope -g quad-zono.cfg -m quad.xml 
spaceex-zonotope -g quad-zono-plot.cfg -m quad.xml -o out.gen 
graph -T png -C -B -q0.5  --bitmap-size 1024x1024 out.gen > quad_zono_plot.png 
cd ..

