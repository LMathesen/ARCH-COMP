This class extends linear systems by uncertain parameters that are fixed over time: 
\begin{equation} \label{eq:linearParametricSystem}
	\dot{x}(t) = A(p) x(t) + \tilde{u}(t), \quad x(0)\in \mathcal{X}_O \subset \mathbb{R}^n, \quad p\in \mathcal{P}, \quad \tilde{u}(t)\in \tilde{\mathcal{U}} = \{B(p) \otimes \mathcal{U} | \mathcal{U} \subset \mathbb{R}^n, p\in \mathcal{P} \},
\end{equation} 
The set of state and input matrices is denoted by 
\begin{equation} \label{eq:linearParamSystems_matrices}
 \mathcal{A} = \{A(p) | p \in \mathcal{P}\}, \quad \mathcal{B} = \{B(p) | p \in \mathcal{P}\}
\end{equation}
An alternative is to define each parameter as a state variable $\tilde{x}_i$ with the trivial dynamics $\dot{\tilde{x}}_i = 0$. The result is a nonlinear system that can be handled as described in Sec. \ref{sec:nonlinearSystems}. The problem of which approach to use for any particular case is still open.

Since the \texttt{linParamSys} class is written using the new structure for object oriented programming in MATLAB, it has the following public properties:
\begin{itemize}
 \item \texttt{A} -- set of system matrices $\mathcal{A}$, see \eqref{eq:linearParamSystems_matrices}. The set of matrices can be represented by any matrix set introduced in Sec. \ref{sec:matrixSetRepresentationsAndOperations}.
 \item \texttt{B} -- set of input matrices $\mathcal{B}$, see \eqref{eq:linearParamSystems_matrices}. The set of matrices can be represented by any matrix set introduced in Sec. \ref{sec:matrixSetRepresentationsAndOperations}.
 \item \texttt{stepSize} -- constant step size $t_{k-1} - t_k$ for time intervals of the reachable set computation.
 \item \texttt{taylorTerms} -- number of Taylor terms for computing the matrix exponential, see \cite[Theorem 3.2]{Althoff2010a}.
 \item \texttt{mappingMatrixSet} -- set of exponential matrices, see Sec. \ref{sec:matrixSetRepresentationsAndOperations}.
 \item \texttt{E} -- remainder of matrix exponential computation.
 \item \texttt{F} -- uncertain matrix to bound the error for time interval solutions, see e.g. \cite[Proposition 3.1]{Althoff2010a}.
 \item \texttt{inputF} -- uncertain matrix to bound the error for time interval solutions of inputs, see e.g. \cite[Proposition 3.4]{Althoff2010a}.
 \item \texttt{inputCorr} -- additional uncertainty of the input solution if origin is not contained in input set, see \cite[Equation 3.9]{Althoff2010a}.
 \item \texttt{Rinput} -- reachable set of the input solution, see Sec. \ref{sec:linearSystems}.
 \item \texttt{Rtrans} -- reachable set of the input $u_c$, see Sec. \ref{sec:linearSystems}.
 \item \texttt{RV} -- reachable set of the input $\tilde{\mathcal{U}}_\Delta$, see Sec. \ref{sec:linearSystems}.
 \item \texttt{sampleMatrix} -- possible matrix $\hat{A}$ such that $\hat{A} \in \mathcal{A}$.
\end{itemize}

\subsubsection{Method \texttt{initReach}} \label{sec:linearParamSystem_initReach}

The method \texttt{initReach} computes the reachable set for the first point in time $r$ and the first time interval $[0,r]$ similarly as for linear systems with fixed parameters. The main difference is that we have to take into account an uncertain state matrix $\mathcal{A}$ and an uncertain input matrix $\mathcal{B}$. The initial state solution becomes
\begin{equation}\label{eq:homParamLinear}
	\mathcal{R}^d_h = e^{\mathcal{A} r}\mathcal{X}_O = \{ e^{A r} x_0| A \in \mathcal{A}, x_0 \in \mathcal{X}_O \}.
\end{equation}
Similarly, the reachable set due to the input solution changes as described in \cite[Section 3.3]{Althoff2010a}. The following private functions take care of the required computations:
\begin{itemize}
 \item \texttt{mappingMatrix} -- computes the set of matrices which map the states for the
next point in time according to \cite[Section 3.1]{Althoff2011b}.
 \item \texttt{tie} (\textbf{t}ime \textbf{i}nterval \textbf{e}rror) -- computes the error made by generating the convex hull of reachable sets of points in time for the reachable set of the corresponding time interval as described in \cite[Section 3.2]{Althoff2011b}. 
 \item \texttt{inputSolution} -- computes the reachable set due to the input according to the superposition principle of linear systems. The computation is performed as suggested in \cite[Theorem 1]{Althoff2011b}. 
\end{itemize}