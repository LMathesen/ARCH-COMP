function corrTraj = removeTrackingErrors(fullTraj, incMat)
% removeTrackingErrors - checks whether the distances between body joints
% stay constant within a certain error margin. The markers are often badly
% tracked, or not tracked at all. If they are not tracked at all, their 
% values are often [0 0 0]. Some kind of Kalman-like-filtering is used in 
% the tracking software, so if the markers are occluded, they just continue 
% on their trajectory (i.e. the prediction step of the Kalman filter 
% without the update step). If they are occluded for a long time, they just 
% keep on travelling and "fly away" from the other markers.  
% 
%
% Syntax:  
%    traj_corrected = findTrackingErrors(traj,correctionFlag)
%
% Inputs:
%    traj - recorded trajectory of a marker
%    correction_flag - 1 if trajectory should be automatically
%
% Outputs:
%    t_violation - time relative to the start time of first violation; inf
%    is returned if no violation took place.
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      10-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------



% loop through all markers
for iMarker = 1:length(incMat)

    % extract trajectory
    markerTraj = fullTraj(:,make3(iMarker));

    % find neighbors of current marker
    neighbor = find(incMat(iMarker,:) == 1);

    % loop through all neighbors
    for i = 1:length(neighbor)

        % extract trajectory
        neighborTraj = fullTraj(:,make3(neighbor(i)));

        % compute difference
        diff = markerTraj - neighborTraj;

        % obtain distance
        distance = vecnorm(diff,2,2);

        % detect outlier
        %ind(i,:) = isoutlier(distance,'mean'); 
        ind(i,:) = isoutlier(distance,'movmedian',20); 

%         % plot
%         distanceBefore(:,i)=distance;
%         figure
%         hold on
%         plot(distance)
%         plot(find(ind(i,:)==1),distance(ind(i,:)),'ro')
    end
    % obtain indices when all distances to neighbors deviate
    indComb = find(sum(ind)>=2);
    % save old values
    oldMarkerTraj = markerTraj;
%     oldIndComb = indComb;
    while ~isempty(indComb)
        % counter
        counter = 2;
        % interpolate
        firstInd = indComb(1);

        if length(indComb)>1
            % check when signal is normal again
            while counter <= length(indComb) && indComb(counter) == indComb(counter-1)+1
                counter = counter+1;
            end
            % set last index
            lastInd = indComb(counter-1);
            interpolateDist = counter-1;
        else
            % last index equals first index
            lastInd = firstInd;
            interpolateDist = 1;
        end

        % delta 
        delta = (oldMarkerTraj(lastInd,:) - oldMarkerTraj(firstInd,:))/interpolateDist;

        % replace values
        if firstInd > 1 % repair in the middle of a trajectory
            for i = 1:interpolateDist
                markerTraj(firstInd+i-1,:) = oldMarkerTraj(firstInd-1,:) + i*delta;
            end
        else % first value is already erroneous
            for i = 1:interpolateDist
                markerTraj(firstInd+i-1,:) = oldMarkerTraj(lastInd+1,:); % take value of first correct value
            end
        end

        % remove indices
        indComb(1:counter-1) = [];
    end
%     % check result
%     for i = 1:length(neighbor)
%         neighborTraj = fullTraj(:,make3(neighbor(i)));
%         diff = markerTraj - neighborTraj;
%         distance = vnorm(diff,2);
%         figure
%         hold on
%         plot(distanceBefore(:,i))
%         plot(find(ind(i,:)==1),distanceBefore(ind(i,:),i),'ro')
%         plot(oldIndComb,distanceBefore(oldIndComb,i),'ko')
%         plot(distance,'g');
%     end
    
    % store in corrected trajectory
    corrTraj(:,make3(iMarker)) = markerTraj;
end

function y = make3(x)
    %returns indices of a given marker; each marker has three coordinates
    y = reshape([3*x-2;3*x-1;3*x],1,3*length(x));
end

end

%------------- END OF CODE --------------