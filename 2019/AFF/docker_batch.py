import os
import string
import re
from subprocess import Popen, PIPE

# batch run for a category
# example using 2019 AFF category (subset of participants)
#
# 1) set up docker containers
#    in subfolders, for example, 
#    cd SpaceEx
#    docker build . -t spaceex
#    cd ../Hylaa
#    docker build . -t hylaa
#    repeat for other tools/containers
# 2) run python docker_batch.py
#    or python3 depending on host system set up
#
#    this may take a bit, expected output of the form
#
#    total results (corresponding to each example in order):
#    {'spaceex': [['SpaceStation', 'ISSF01-ISS01', 'ISSF01'], ['SpaceStation', 'ISSF01-ISU01', 'ISSF01'], ['SpaceStation', 'ISSC01-ISS02', 'ISSC01'], ['SpaceStation', 'ISSC01-ISU02', 'ISSC01']], 'juliareach': [], 'hylaa': [['/re/space_station/', 'ISSF01.py safe'], ['/re/space_station/', 'ISSF01.py unsafe'], ['/re/space_station/', 'ISSC01.py safe'], ['/re/space_station/', 'ISSC01.py unsafe']]}
#    {'spaceex': [51.0261, 49.9561, 33.2401, 32.9711], 'juliareach': [], 'hylaa': [325.5, 194.6, 43.78, 1.49]}
#
# most of this is set up to run several calls to the docker 
# container (e.g., multiple docker runs, typically one for 
# each example), although variations are possible
# for the example, only spaceex and hylaa are tested fully, 
# juliareach was also attempted, but ran into some piping 
# issues I think within julia itself, although the 
# juliareach code gives a rough example of how a 
# tool that runs everything inside 1 docker run call 
# could be parsed

# all of this of course would be simpler e.g. with 
# producing a csv, json, etc. of results

# for sharing paths between host and container
string_path_host = os.getcwd()

# tools specified, can generalize loops below
tools = ['spaceex', 'hylaa', 'juliareach']

# examples organized with input/output file/string results for parsing runtime results
# todo: can reorganize this if desired, for now, just set up with file/result names for each tool
example = {'spaceex': [], 'juliareach': [], 'hylaa': []}
example['spaceex'].append(['SpaceStation', 'ISSF01-ISS01', 'ISSF01'])
example['spaceex'].append(['SpaceStation', 'ISSF01-ISU01', 'ISSF01'])
example['spaceex'].append(['SpaceStation', 'ISSC01-ISS02', 'ISSC01'])
example['spaceex'].append(['SpaceStation', 'ISSC01-ISU02', 'ISSC01'])

example['hylaa'].append(['/re/space_station/', 'ISSF01.py safe'])
example['hylaa'].append(['/re/space_station/', 'ISSF01.py unsafe'])
example['hylaa'].append(['/re/space_station/', 'ISSC01.py safe'])
example['hylaa'].append(['/re/space_station/', 'ISSC01.py unsafe'])

# skipping remainder due to runtime / getting this example out, but representative examples

#example['spaceex'].append(['Rendezvous', 'SRNA01-SR02', 'SRNA01-SR0_'])
#example['spaceex'].append(['Rendezvous', 'SRA01-SR02', 'SRA01-SR0_'])
#example['spaceex'].append(['Rendezvous', 'SRU01-SR02', 'SRU01-SR0_'])
#example['spaceex'].append(['Rendezvous', 'SRU02-SR02', 'SRU02-SR0_'])

#example['spaceex'].append(['Building', 'BLDC01-BDS01', 'BLDC01'])
#example['spaceex'].append(['Building', 'BLDF01-BDS01', 'BLDF01'])

#example['spaceex'].append(['Platoon', 'PLAD01-BND42', 'PLAD01-BND'])
#example['spaceex'].append(['Platoon', 'PLAD01-BND30', 'PLAD01-BND'])
#example['spaceex'].append(['Platoon', 'PLAN01-UNB50', 'PLAN01-UNB'])

#example['spaceex'].append(['Gear', 'GRBX01-MES01', 'GRBX01-MES01'])
#example['spaceex'].append(['Gear', 'GRBX01-MES02', 'GRBX01-MES01'])

# spaceex base command
# 'docker run  -v ', string_path, ':`pwd` -w `pwd` spaceex -g $1$2.cfg -m $1$3.xml -vl | grep 'Forbidden\|Computing reachable states done after'

# spaceex interactive, note entrypoint required
#string_cmd = 'docker run -it --entrypoint /bin/bash spaceex'

# juliareach base command
# echo AFF | docker run  -v `pwd`:/result -i -w "/juliareach" juliareach julia startup.jl

# hylaa base command
# docker run -w '/re/space_station/' hylaa python3 /re/space_station/ISSC01.py safe

# output result time list
# can generalize to have paths for figures, multiple runs (discrete vs. continuous for example), etc.
output_time_l = {'spaceex': [], 'juliareach': [], 'hylaa': []}

# some tools provide a script to run everything at once
# whereas some tools provide a command line call to run each 
# example
#
# if they provide an individual script that runs everything, 
# don't iterate over all examples with calls and instead make 
# 1 call and parse results produced for all examples
# note this is set up to still iterate over all examples
breakout_examples = 0

# some results in ./results
if not os.path.isdir('result'):
	os.mkdir('result')

# iterate over all tools and all examples for each tool
for t in tools:
	for i in range(0,len(example[t])):
		# set up command line call for each tool
		if t == 'spaceex':
			string_cmd = 'docker run  -v ' + string_path_host + '/SpaceEx/:/root/ -w /root spaceex -g ' + example[t][i][0] + '/' + example[t][i][1] + '.cfg -m ' + example[t][i][0] + '/' + example[t][i][2] + '.xml' + ' -vl'
			breakout_examples = 0
		elif t == 'juliareach':
			break # TODO: skip due to result parsing issues
			# note: no space after AFF in next is important
			# original call for all examples
			#string_cmd = 'echo AFF| docker run  -v ' + string_path_host + '\result:/result -i -w \"/juliareach\" juliareach julia startup.jl'
			# shorter call with just ISS
			# note in next: windows requires the arguments 
			# following -e to be in "" quotes whereas it 
			# may need to be '' quotes on unix
			#
			# TODO: ran into some issues with piping between 
			# julia and host, so gave up on this and added 
			# short hylaa example instead, would need to be 
			# generalized
			string_cmd = 'docker run  -v ' + string_path_host + ':/result -w \"/juliareach\" juliareach julia -e \"ENV[\\\"GKSwstype\\\"] = \"100\"; import Pkg; Pkg.activate(@__DIR__); Pkg.instantiate(); pwd(); include(\\\"models/AFF/SpaceStation/iss_benchmark.jl\\\")\"'
			breakout_examples = 1
		elif t == 'hylaa':
			string_cmd = 'docker run -w \"' + example[t][i][0] + '\" hylaa python3 ' + example[t][i][0] + example[t][i][1]
			breakout_examples = 0
		else:
			print('ERROR: tool not indicated')
			break

		# always run on first example (for tools that make 1 call to generate all results)
		# run each time (for tools that make individual calls repeatedly)
		# note that we still parse for each example
		if breakout_examples == 0 or i == 0:
			print('CALLING: ', string_cmd)

			# call system command via subprocess for pipe redirection
			p = Popen(string_cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE)
			#, shell=True) # last seemed necessary for julia

# line-by-line variant
#			output = ''
#			idx_o = 0
#			while True:
#				line = p.stdout.readline()
#				if not line:
#					break
#				#the real code does filtering here
#				print(line.rstrip())
#				idx_o += 1
#				output += str(line.rstrip())
#				
#				if idx_o >= 35:
#					break

			# get stdout as string
			# TODO/note: may be better/safer to use 
			# p.communicate, but this seems to work
			#output = str(p.stdout.read())
			output = str(p.communicate())
			
			print(output)

		# search string in stdout
		# note: this is not at all robust, would be 
		# great to have each tool output a table (csv, 
		# json, etc.) with results for each example, 
		# configuration, path to figure(s), etc. to 
		# be more robust
		if t == 'spaceex':
			fstr = 'Computing reachable states done after'
			r_idx = output.find(fstr)
			r_idx_time_start = int(r_idx)+len(fstr)+1
			r_idx_time_end = int(r_idx)+len(fstr)+8
		elif t == 'juliareach':
			# todo: generalize for each example via string list examples
			fstr = '(\"ISS01\", \"dense\") => TrialEstimate('
			r_idx = output.find(fstr)
			r_idx_time_start = int(r_idx)+len(fstr)
			r_idx_time_end = int(r_idx)+len(fstr)+4
		elif t == 'hylaa':
			fstr = 'Total Runtime:'
			r_idx = output.rfind(fstr) # search from end
			r_idx_time_start = int(r_idx)+len(fstr)+1
			r_idx_time_end = int(r_idx)+len(fstr)+6

		# r_idx non-negative if found
		if r_idx >= 0:
			output_time = output[r_idx_time_start:r_idx_time_end];
			print("output_time_before_re: ", output_time)
			# remove non-numeric characters (except .)
			output_time = re.sub("[^\d\.]", "", output_time)
			print(output_time)
			output_time_l[t].append(float(output_time))
		else:
			print('WARNING: search string not found in output, check result order for consistency')

		print(r_idx)
		print(output_time_l)

print('total results (corresponding to each example in order):')
print(example)
print(output_time_l)
